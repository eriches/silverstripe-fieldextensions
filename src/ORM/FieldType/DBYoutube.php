<?php
namespace Hestec\FieldExtensions\ORM\FieldType;
use SilverStripe\ORM\FieldType\DBVarchar;

class DBYoutube extends DBVarchar
{

    public function __construct($name = null, $size = 255)
    {
        parent::__construct($name, $size);
    }

    public function getCode(){

        $url = $this->value;

        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
        if (isset($match[1]) && strlen($match[1]) > 5){
            return $match[1];
        }
        return false;

    }

}